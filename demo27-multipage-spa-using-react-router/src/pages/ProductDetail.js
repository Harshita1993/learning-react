import { useParams } from "react-router-dom";

const ProductDetail = () => {
  const params = useParams();

  //Keys are the dynamic segments leading to a particular page
  console.log(params.productId);

  return (
    <section>
      <h1>Product Detail</h1>
      <p>{params.productId}</p>
    </section>
  );
};

export default ProductDetail;