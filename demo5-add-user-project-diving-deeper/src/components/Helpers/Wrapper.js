//does nothing but just return props children. But this is enough to fulfill this requirement JSX has. 
const Wrapper = props => {
  return props.children;
};

export default Wrapper;