import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import "./index.css";
import App from "./App";
import store from "./store/index";

ReactDOM.render(
  //This prop now provides our store to the react app
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
