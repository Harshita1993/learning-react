import { createSlice } from '@reduxjs/toolkit';

const initialCounterState = { counter: 0, showCounter: true };

//Slice of our global state
const counterSlice = createSlice({
  name: 'counter',
  initialState: initialCounterState,
  //Map of all the reducers this state slice needs
  //Every method here will automatically receive the latest state
  //These are automatically called depending on the action triggered
  reducers: {
    //we are allowed to mutate state here
    increment(state){
      state.counter++;
    },
    decrement(state){
      state.counter--;
    },
    increase(state, action){
      state.counter = state.counter + action.payload;
    },
    toggleCounter(state){
      state.showCounter = !state.showCounter;
    }
  }
});

//createSlice automatically creates unique identifiers for the different reducers
//this is how we access, but we are not accessing the reducer methods defined above
//instead, we get methods created automatically by redux-toolkit which when called, will create action objects for us
//returns an action object of this shape
// {type: 'some auto-generated unique identifier'}
//these are called action creator methods
//counterSlice.actions.toggleCounter
export const counterActions = counterSlice.actions;

export default counterSlice.reducer;