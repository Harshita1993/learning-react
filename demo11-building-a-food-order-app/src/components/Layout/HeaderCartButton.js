import { useContext, useEffect, useState } from 'react';

import classes from './HeaderCartButton.module.css';
import CartContext from '../../store/cart-context';
import CartIcon from "../Cart/CartIcon";

const HeaderCartButton = props => {
  const [btnIsHighlighted, setBtnIsHiglighted] = useState(false);
  const cartCtx = useContext(CartContext);

  const { items } = cartCtx

  const numberOfCartItems = items.reduce((currentNumber, item) => {
    return currentNumber + item.amount;
  }, 0);

  const btnClasses = `${classes.button} ${btnIsHighlighted ? classes.bump: ''}`;

  useEffect(() => {
    //Change button classes to include bump animation, and a timer to remove it again
    if(items.length === 0){
      return;
    }
    setBtnIsHiglighted(true);
    
    const timer = setTimeout(() => {
      setBtnIsHiglighted(false);
    }, 300);

    return () => {
      clearTimeout(timer);
    };
  }, [items]);

  return (
    <button className={btnClasses} onClick={props.onClick}>
      <span className={classes.icon}>
        <CartIcon />
      </span>
      <span>Your Cart</span>
      <span className={classes.badge}>{numberOfCartItems}</span>
    </button>
  );
}

export default HeaderCartButton;