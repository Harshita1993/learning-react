import React, { useRef, useImperativeHandle } from "react";

import classes from './Input.module.css';

const Input = React.forwardRef((props, ref) => {
  const inputRef = useRef();

  //it runs only after the component is executed the first time
  //using it to focus
  // useEffect(() => {
  //   //focus() built into input DOM objects
  //   inputRef.current.focus();
  // },[]);

  const activate = () => {
    inputRef.current.focus();
  };

  //Exposing this function from a react component to its parent component
  useImperativeHandle(ref, () => {
    return {
      focus:activate
    }
  });

  return (
    <div
      className={`${classes.control} ${
        props.isValid === false ? classes.invalid : ""
      }`}
    >
      <label htmlFor={props.id}>{props.label}</label>
      <input
        ref={inputRef}
        type={props.type}
        id={props.id}
        value={props.value}
        onChange={props.onChange}
        onBlur={props.onBlur}
      />
    </div>
  );
});

export default Input;
