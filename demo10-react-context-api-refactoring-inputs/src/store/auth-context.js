import React, { useState, useEffect } from 'react';

//Creates an object, it takes a default context
//it returns a component or it will be an object that also contains components
//while AuthContext itself is not a component, it is an object that will contain a component
const AuthContext = React.createContext({
  isLoggedIn: false,
  onLogout: () => {},
  onLogin: (email, password) => {}
});

export const AuthContextProvider = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  //This is executed after every component re-evaluates, to avoid an infinite loop and does not run for every component re-render cycle
  useEffect(() => {
    const storedUserLoggedInInfo = localStorage.getItem("isLoggedIn");
    if (storedUserLoggedInInfo === "1") {
      setIsLoggedIn(true);
    }
  }, []);
  
  const logoutHandler = () => {
    localStorage.removeItem("isLoggedIn");
    setIsLoggedIn(false);
  };

  const loginHandler = () => {
    localStorage.setItem("isLoggedIn", "1");
    setIsLoggedIn(true);
  };
  
  return (
    <AuthContext.Provider 
      value={{
        isLoggedIn: isLoggedIn,
        onLogout: logoutHandler,
        onLogin: loginHandler
      }}
    >
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthContext;

//to use the context in the app, we need to do 2 things
//1. we need to provide (wrap it in JSX code) it, which basically tells React, 'here's my context'
// and all the components that are wrapped by it should have access to it
// after providing, we need to consume it.
//Any component that is not wrapped will not be able to listen