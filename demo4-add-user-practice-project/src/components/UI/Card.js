import React from 'react';

import classes from './Card.module.css';

const Card = props => {
  //props.children = gives us the content that is passed between the opening and closing tag of the Card component
  return (
    <div className={`${classes.card} ${props.className}`}>
      {props.children}
    </div>
  );
};

export default Card;