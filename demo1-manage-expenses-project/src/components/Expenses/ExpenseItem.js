import "./ExpenseItem.css";
import Card from '../UI/Card';
import ExpenseDate from "./ExpenseDate";
//import React, { useState } from 'react';

//props holds all the attributes added in the custom elements in App.js
const ExpenseItem = (props) => {
  //using array destructuring, always returns an array
  //1st element - special variable with value, 2nd - the updating function
  /*const [title, setTitle] = useState(props.title);
  const clickHandler = () => {
    set new value, state updating fn. It is managed by react somewhere in memory.
    By calling this, it is not only updating the value.
    The component in which we called this useState, and in which u initialised with useState will be executed again
    setTitle('Updated!');
  };*/

  return (
    <li>
      <Card className="expense-item">
        <ExpenseDate date={props.date}/>
        <div className="expense-item__description">
          <h2>{props.title}</h2>
          <div className="expense-item__price">${props.amount}</div>
        </div>
      </Card>
    </li>
  );
}

export default ExpenseItem;
