import './Card.css';
import React from 'react';

const Card = (props) => {
  //Any class name received from outside is added to the string 
  const classes = 'card ' + props.className;
  return <div className={classes}>{props.children}</div>;
}

export default Card;