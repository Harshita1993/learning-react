import ProductItem from './ProductItem';
import classes from './Products.module.css';

const DUMMY_PRODUCTS = [
  {
    id: 'p1',
    price: 6,
    title: 'Harry Potter and the Philosopher stone',
    description: 'It is about the boy who lived' 
  },
  {
    id: 'p2',
    price: 5,
    title: 'Harry Potter and the Chamber of Secrets',
    description: 'The boy who lived and the man who tried to kill him' 
  },
  {
    id: 'p3',
    price: 4,
    title: 'Harry Potter and the Prisoner of Azkaban',
    description: 'The boy who lived and his long lost godfather' 
  }
]

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map((product) => (
          <ProductItem
            key={product.id}
            id={product.id}
            title={product.title}
            price={product.price}
            description={product.description}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
