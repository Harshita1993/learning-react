const redux = require('redux');

//add reducer function
//current state and dispatched action are params
const counterReducer = (state = { counter: 0 }, action) => {
  if(action.type === 'increment'){
    return {
      counter: state.counter + 1
    };
  }

  if(action.type === 'decrement'){
    return {
      counter: state.counter - 1
    };
  }

  return state;
};

//store should know which reducer function
const store = redux.createStore(counterReducer);

const counterSubscriber = () => {
  //gives us the latest state snapshot after the state was updated
  const latestState = store.getState();
  console.log(latestState);
};

store.subscribe(counterSubscriber);

//create and dispatch an action
store.dispatch({type: 'increment'});
store.dispatch({type: 'decrement'});