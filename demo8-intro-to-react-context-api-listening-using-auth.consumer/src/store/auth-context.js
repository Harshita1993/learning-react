import React from 'react';

//Creates an object, it takes a default context
//it returns a component or it will be an object that also contains components
//while AuthContext itself is not a component, it is an object that will contain a component
const AuthContext = React.createContext({
  isLoggedIn: false
});

export default AuthContext;

//to use the context in the app, we need to do 2 things
//1. we need to provide (wrap it in JSX code) it, which basically tells React, 'here's my context'
// and all the components that are wrapped by it should have access to it
// after providing, we need to consume it.
//Any component that is not wrapped will not be able to listen