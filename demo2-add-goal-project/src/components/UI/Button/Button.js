import React from 'react';
//This is to transform the code so CSS modules work
import styles from './Button.module.css';
//import styled from "styled-components";

//This is called a tagged template, it is a default js feature,
//button is a method on styled object, instead of adding () we add ``,
//it will be executed as a method, whatever we pass inbetten the back ticks will be passed to the button method
/*const Button = styled.button` --Part of styled-components
  width: 100%;
  font: inherit;
  padding: 0.5rem 1.5rem;
  border: 1px solid #8b005d;
  color: white;
  background: #8b005d;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.26);
  cursor: pointer;

  @media(min-width: 768px){
    width: auto;
  }

  &:focus {
    outline: none;
  }

  &:hover,
  &:active {
    background: #ac0e77;
    border-color: #ac0e77;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.26);
  }
`;*/

const Button = props => {
  return (
    <button type={props.type} className={styles.button} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default Button;
