import { Component } from 'react';

import User from './User';
import classes from './Users.module.css';

class Users extends Component {
  constructor(){
    //state is always an object in class based components
    super();
    this.state = {
      showUsers: true
    };
  }

  //Error Boundary
  componentDidUpdate() {
    // try {
    //   someCodeWhichMightFail()
    // } catch (error) {
    //   //handle error
    // }
    if(this.props.users.length === 0){
      throw new Error('No users provided');
    }
  }

  toggleUsersHandler(){
    //this.state.showUsers = false // Not!
    //React will merge this obj with the existing state, 
    //so the prev state still remains as it is not overridden unlike useState
    this.setState((curState) => {
      return { showUsers: !curState.showUsers };
    });
  }

  render() {
    const usersList = (
      <ul>
        {this.props.users.map((user) => (
          <User key={user.id} name={user.name} />
        ))}
      </ul>
    );

    return (
      <div className={classes.users}>
        <button onClick={this.toggleUsersHandler.bind(this)}>
          {this.state.showUsers ? 'Hide' : 'Show'} Users
        </button>
        {this.state.showUsers && usersList}
      </div>
    );
  }
}

// const Users = () => {
//   const [showUsers, setShowUsers] = useState(true);

//   const toggleUsersHandler = () => {
//     setShowUsers((curState) => !curState);
//   };

//   const usersList = (
//     <ul>
//       {DUMMY_USERS.map((user) => (
//         <User key={user.id} name={user.name} />
//       ))}
//     </ul>
//   );

//   return (
//     <div className={classes.users}>
//       <button onClick={toggleUsersHandler}>
//         {showUsers ? 'Hide' : 'Show'} Users
//       </button>
//       {showUsers && usersList}
//     </div>
//   );
// };

export default Users;
