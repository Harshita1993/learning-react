import { Component } from 'react';

import classes from './User.module.css';

class User extends Component {
  componentWillUnmount() {
    console.log('User will unmount');
  }
  
  //specific method expected by reatc, which it will call when it finds a component being used in the JSX code
  render() {
    return <li className={classes.user}>{this.props.name}</li>;
  } 
}

// const User = (props) => {
//   return <li className={classes.user}>{props.name}</li>;
// };

export default User;
