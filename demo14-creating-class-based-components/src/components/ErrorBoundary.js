import { Component } from 'react';

class ErrorBoundary extends Component{
  constructor(){
    super();
    this.state = { hasError: false };
  }
  
  //Can be added to any class based components, 
  //whenever this is added it makes that class based comp an error boundary
  componentDidCatch(error) {
    console.log(error);
    this.setState({ hasError: true});
  }

  render() {
    if(this.state.hasError){
      return <p>Something went wrong</p>;
    }
    
    //Because to wrap all the components that should be protected by Error Boundary
    return this.props.children;
  }
}

export default ErrorBoundary;